# Project RC Gun
## Purpose
This project captures my progress in making a remote controlled (Nerf) gun using a micro controller. 
I already had a first version working (April 2020) with an Arduino Uno. But both the mechanical design as well as the software could use some upgrades.
I also wanted to change the microcontroller to the ESP32.
This document will primarily go over the software, but the mechanical concepts will be briefly explained, hopefully making it easier
to follow this project. 

I hope there will be aspects covered here to inspire you or help you build your own project.
Because I am doing this as a hobby, the code and concepts in this project may be of low quality.  

## Mechanical design
Two stepper motors are used to rotate horizontally and vertically. The stepper motor for the vertical rotation is connected to a gear 
transmission to increase precision. The stepper motor accountable for the horizontal rotation is directly mounted to the platform. On second
thought this might not be a good idea, as the total weight of the platform falls on the rotor of the motor. Because the platform isn't 
heavy, I won't change this.   

The two steppers are driven by the DRV8825 motor drivers. These will get very hot. The data sheet tells us the drivers can reach temperatures
of 150°C. Because I'm uncomfortable letting these drivers reach such temperatures, I installed a fan to cool them.

The actual shooting mechanism can be found on the platform. I modified the mechanism of an old Nerf gun I had laying around so it could
be mounted on the turret. The shooting action happens with two DC motors. These will be driven by the L298N motor driver.

If you would want to build something like this, I would recommend using servo motors. They are way easier to use and would also fit the job.
I used a 3D printer (Creality ender 3 Pro) to print the smaller pieces as well as the gears. However, it would be far more efficient 
to use a lasercutter or CNC router for the gears.

Pictures and videos of the turret can be found in the folder 'Mechanics' above.
	
## Software
~I will be using Windows 10, Python 3.8 or higher (on Pycharm) and Visual Studio Code with Platformio plugin~

I wanted to write a programme to control the microcontroller because I think it is more powerful than using a second one as remote controller (which I did in the
first version). I chose Python simply because it's easy to use.

First we need a bluetooth connection between python and the ESP32. Connect the ESP32 to you computer over bluetooth.
To check which Bluetooth COM ports are opened, navigate to Bluetooth settings and click 'More Bluetooth options'. There, you will hopefully see something like this:

<img src="Images/Bluetooth-screenshot.png"  width="357" height="492">  

Now that we know the outgoing port (COM4 for me, but it can change), we can use the pySerial library to write to the port.
I would suggest trying to send a single string from your computer to ESP32 to see if it works.

### ESP32
The code on the ESP32 is written using FreeRTOS which, in my opinion, makes it easier to control stepper motors and gives the opportunity to run different tasks 'at the same time'.
Note that this code cannot be used on an Arduino microcontroller because I used the two cores on the ESP32 (Arduino only has one).

One core receives the incomming string from the computer (see next section) and assigns it to global variables using mutexes.
The other core uses these global variables to control the turret.

When the turret is powered, it wil start a homing sequence, once this is done it will wait for the bluetooth connection. 
If it is connected to the pc we can start to control it.

### Computer
To make a string object containing the info to control the turret, we first need a way to make that info, i.e. 'how do we want to control the turret?'
We could use mouse and keyboard, a joystick, Xbox controller,... I chose a Logitech Attack3 joystick I had laying around.
I found the best way to use this script is to open it via the terminal or right-clock the file and click 'open with python', not with an IDE.

Currently this is how the buttons are configured:
- Joystisck lef/right:	turret left/right
- Joystock up/down:	turret up/down
- Scroll wheel:		control speed of bullets
- Button 1:		Fire turret
- Button 2:		Toggle motor to give bullets speed
- Button 8:		Boost mode (experimental)
- Button 9:		Homing sequence
- Button 10:		enable/disable all motors
- Button 11:		Stop python script

The files for the ESP32 and the computer can be found in the 'Software' folder.

Warning: The software on the ESP32 is still to be optimised.

## Electronics
I designed the electric circuit using Keycad. A pdf of the circuit and photos of the soldered board can be found in the folder 'Electronics'.


Warning: The turret had worked for a short time, unfortunately I tried to increase the current limit on a stepper driver and accidentely fried it (always unplug the motor when changing Vref, M0,M1,M2, Vmot,..).
Because of this I haven't had the chance to troubleshoot thoroughly. There may be bugs in the software and electronic design.
I will add more photos and videos once I obtained a new stepper motor driver and had the chance to finish this project.

## Todo
- Push Kicad files to git
- Change data transmission between computer and ESP32 from string to struct
