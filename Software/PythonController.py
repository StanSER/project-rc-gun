import serial
import pygame
import time
import sys
import os

def joystick():
    sys.stdout.write("\n---Joystick---")
    sys.stdout.write("\nInitialising joystick...")
    try:
        pygame.init()  # Initialize Pygame Module
        mod_pad = pygame.joystick
        mod_pad.init()  # Initialize joystick func
        if mod_pad.get_init() == 1:
            sys.stdout.write("\nInitialisation Success.")

        pad_cnt = pygame.joystick.get_count()
        sys.stdout.write("\nNow [%d] Devices Connected." %pad_cnt)

        controller = mod_pad.Joystick(0)  # Create a new Joystick object

        controller.init()  # Initialize Controller

        if controller.get_init() == 1:
            sys.stdout.write("\nController initialisation Success.")

        return controller
    except:
        sys.stdout.write("\nError occurred during joystick initialisation.")
        sys.stdout.write("\nMake sure the joystick is plugged in.")
        sys.stdout.write("\nRestart required.\n")
        for x in [5, 4, 3, 2, 1]:
            sys.stdout.write("\rRestarting in %d " %x)
            sys.stdout.flush()
            time.sleep(1)
        sys.stdout.write("Openening: ")
        os.system(__file__)


def bluetooth(controller):
    # Make object 'esp' allocated to the outgoing (bluetooth) port 'COMX'
    # and set baudrate.
    sys.stdout.write("\n\n---Bluetooth Connection---")
    sys.stdout.write("\nConnecting to ESP32...")

    try:
        esp = serial.Serial('COM4')
        esp.baudrate = 921600
        sys.stdout.write("\nESP32 connected.")
    except:
        sys.stdout.write("\nError occurred while connecting to ESP32.")
        sys.stdout.write("\nMake sure ESP32 is turned on and ready to connect over bluetooth.\n")
        for x in [5, 4, 3, 2, 1]:
            sys.stdout.write("\rRetrying in %d " %x)
            sys.stdout.flush()
            time.sleep(1)
        bluetooth(controller)

    sys.stdout.write("\n\nWaiting for initiating of ESP32...")

    # Wait for initiation of ESP32
    while True:
        bytes_in = esp.inWaiting()
        if esp.read(bytes_in) == b'ESP ready':
            break

    esp.write(" ".encode())

    sys.stdout.write("\n\nCommunication initialized.")

    ## Code to make and fill data object
    btn10 = int(controller.get_button(10))
    sys.stdout.write("\n\nPress button 11 on joystick to terminate programme.\n")
    sys.stdout.write("\nOUTPUT:\n")

    while btn10 != 1:
        pygame.event.pump()

        # map x : -1 tot 1
        # x + 1 => x : 0 tot 2
        # x / 2 * 255
        axis_0 = round((float(controller.get_axis(0)) + 1) / 2 * 255)
        axis_1 = round((float(controller.get_axis(1)) + 1) / 2 * 255)
        axis_2 = abs(round((float(controller.get_axis(2)) + 1) / 2 * 255) - 255)
        btn0 = int(controller.get_button(0))
        btn1 = int(controller.get_button(1))
        btn2 = int(controller.get_button(2))
        btn3 = int(controller.get_button(3))
        btn4 = int(controller.get_button(4))
        btn5 = int(controller.get_button(5))
        btn6 = int(controller.get_button(6))
        btn7 = int(controller.get_button(7))
        btn8 = int(controller.get_button(8))
        btn9 = int(controller.get_button(9))
        btn10 = int(controller.get_button(10))

        data_object = str(axis_0) + ',' + str(axis_1) + ',' + str(axis_2) + ',' + str(btn0) + ',' + str(btn1) + ',' + str(btn2) + ',' + str(btn3) + ',' + str(btn4) + ',' + str(btn5) + ',' + str(btn6) + ','+ str(btn7) + ',' + str(btn8) + ',' + str(btn9) + ','

        sys.stdout.write("\r%s" % data_object)
        sys.stdout.write(" "*len(data_object))
        sys.stdout.flush()

        esp.write(data_object.encode())
        time.sleep(0.040)


    esp.close()
    sys.stdout.write('\n')


def main():
    input("Press enter to begin.")
    controller = joystick()
    bluetooth(controller)
    sys.stdout.write("\nProgram terminated. ")
    input("Press enter to close window.")

main()
