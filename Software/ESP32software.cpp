#include <Arduino.h>
#include <BluetoothSerial.h>
#include <freertos/FreeRTOS.h>

// Bluetooth opbject
BluetoothSerial BlueSerial;

// FreeRTOS handles
TaskHandle_t taskhandle_Bluetooth;
TaskHandle_t taskhandle_Stepper_0;
TaskHandle_t taskhandle_Stepper_1;
TaskHandle_t taskhandle_Axis_2_and_btn;

SemaphoreHandle_t mutex_axis_0;
SemaphoreHandle_t mutex_axis_1;
SemaphoreHandle_t mutex_ax_and_btn;

QueueHandle_t queu_handle_homing_0;
QueueHandle_t queu_handle_homing_1;

// Global variables
byte global_axis_0;
byte global_axis_1;

struct axis_2_and_btn {

    byte axis_2;
    bool btn1;
    bool btn2;
    bool btn3;
    bool btn4;
    bool btn5;
    bool btn6;
    bool btn7;
    bool btn8;
    bool btn9;
    bool btn10;
    bool BL_Connected = false;

} axis_2_and_btn;

// PWM channel declaration
const int PWM_CHANNEL_A = 0; 
const int PWM_CHANNEL_B = 1;

// Pin declarations
#define limit_axis_0_pin 34
#define limit_axis_1_pin 35

#define relais_pin 26

#define stepper_0_stepPin 5
#define stepper_0_dirPin 4
#define stepper_0_sleepPin 12
#define stepper_0_enablePin 13

#define stepper_1_stepPin 27
#define stepper_1_dirPin 14
#define stepper_1_sleepPin 16
#define stepper_1_enablePin 17

#define enable_DC_A_Pin 18
#define enable_DC_B_Pin 19

// ****************************************************************
// Tasks

void initiateBluetoothCommunication() {

    Serial.println("Waiting for bluetooth connection.");

    // Wait for response from controller
    while (BlueSerial.available() == 0) {
        
        BlueSerial.print("ESP ready");
        delay(200);

    }

}

void stepStepper(bool stepperID) {

    if (stepperID == 0) {

        digitalWrite(stepper_0_stepPin, HIGH);
        delayMicroseconds(2);
        digitalWrite(stepper_0_stepPin, LOW);
        delayMicroseconds(2);
        
    } else {

        digitalWrite(stepper_1_stepPin, HIGH);
        delayMicroseconds(2);
        digitalWrite(stepper_1_stepPin, LOW);
        delayMicroseconds(2);
    }

}

void disableAllMotors() {

    // Sleep steppers
    digitalWrite(stepper_0_enablePin, HIGH);     // Logic high to disable
    digitalWrite(stepper_1_enablePin, HIGH);     // Logic high to disable
    digitalWrite(stepper_0_sleepPin, LOW);       // Logic low to disable
    digitalWrite(stepper_1_sleepPin, LOW);       // Logic low to disable

    // Disable DC motors
    ledcWrite(PWM_CHANNEL_A, 0);
    ledcWrite(PWM_CHANNEL_B, 0);
    ledcDetachPin(enable_DC_A_Pin);
    ledcDetachPin(enable_DC_B_Pin);

    Serial.println("disableAllMotors.");

}

void enableAllMotors() {

    // Wake steppers
    digitalWrite(stepper_0_enablePin, LOW);     // Logic low to enable
    digitalWrite(stepper_1_enablePin, LOW);     // Logic low to enable
    digitalWrite(stepper_0_sleepPin, HIGH);     // Logic high to enable
    digitalWrite(stepper_1_sleepPin, HIGH);     // Logic high to enable

    delay(1);

    // Enable DC motors
    ledcAttachPin(enable_DC_A_Pin, PWM_CHANNEL_A);
    ledcAttachPin(enable_DC_B_Pin, PWM_CHANNEL_B);

    // Enable relais
    digitalWrite(relais_pin, HIGH);

    Serial.println("EnableAllMotors.");

}

void homingSteppers() {

    Serial.println("Begin homing stepper.");

    // Suspend tasks to drive steppers
    vTaskSuspend(taskhandle_Stepper_0);
    vTaskSuspend(taskhandle_Stepper_1);

    enableAllMotors();

    // Stepper_0 to home position
    digitalWrite(stepper_0_dirPin, HIGH);

    while (digitalRead(limit_axis_0_pin) == LOW) {
        
        stepStepper(0);
        delay(60);      // Using delay() to stay in this void

    }

    // Serial.println("Stepper 0 home");

    delay(300);

    // Add entry to homing queu so taskStepper_0 knows homing has been done
    bool homed_stepper_0 = true;
    xQueueSendToBack(queu_handle_homing_0,&homed_stepper_0, 0);
    
    // Stepper_1 to home position
    digitalWrite(stepper_1_dirPin, HIGH);

    while (digitalRead(limit_axis_1_pin) == LOW) {

        stepStepper(1);
        delay(60);

    }

    // Serial.println("Stepper 1 home");

    delay(300);

    // Add entry to homing queu so taskStepper_1 knows homing has been done
    bool homed_stepper_1 = true;
    xQueueSendToBack(queu_handle_homing_1, &homed_stepper_1, 0);

    // Change direction of steppers
    digitalWrite(stepper_0_dirPin, LOW);
    digitalWrite(stepper_0_dirPin, LOW);

    // Stepper 0 and 1 to beginning position (at 15 steps = 27, 20 steps = 36 degrees from homing point)
    byte i = 0;
    while (i < 15) {

        stepStepper(0);
        delay(60);
        i++;

    }

    i = 0;
    while (i < 20) {

        stepStepper(1);
        delay(60);
        i++;

    }

    // Resume tasks to drive steppers
    vTaskResume(taskhandle_Stepper_0);
    vTaskResume(taskhandle_Stepper_1);

    Serial.println("Homing steppers finished.");

}

void taskBluetooth(void * parameter){
    
    // Variables for receiving input String
    char tempInput;
    String newString;
    String oldString = "";
    int no_connection_counter = 0;
    bool startUp = true;

    // Variables to process string
    byte i = 0;
    byte j = 0;
    String tempString;
    char tempValue;

    while(true) {
        
        vTaskDelay(30 / portTICK_PERIOD_MS);    // Incomming data send every 0.040s end prevent watchdog from biting

        newString = "";

        // Code to receive incomming data
        while(BlueSerial.available() > 0) {
            tempInput = BlueSerial.read();
            newString += tempInput;
        }

        // Code to process incomming data
        if (newString != "") {      // If there was incoming BlueSerial data 
            
            // Reset no-connection counter
            no_connection_counter = 0;

            // If first time enable all motors
            if (startUp) {

                enableAllMotors();
                startUp = false;

            }

            if (newString != oldString) {       // If data is different from last time
                
                oldString = newString;

                // Code with mutexes to assign global variable
                i= 0;
                j = 0;

                while(i < newString.length()) {

                    if (newString[i] == ',') {

                        tempValue = tempString.toInt();
                        tempString = "";

                        switch (j) {
                        case 0:         // Axis 0 

                            if (xSemaphoreTake(mutex_axis_0, 0) == pdTRUE) {
                                
                                global_axis_0 = tempValue;
                                xSemaphoreGive(mutex_axis_0);

                            }
                            
                            break;

                        case 1:         // Axis 1 
                            
                            if (xSemaphoreTake(mutex_axis_1, 0) == pdTRUE) {
                                
                                global_axis_1 = tempValue;
                                xSemaphoreGive(mutex_axis_1);

                            }

                            break;
                        default:        // Axis 2, btn0 - btn10

                            if (xSemaphoreTake(mutex_ax_and_btn, 0) == pdTRUE) {

                                axis_2_and_btn.BL_Connected = true;
                                
                                switch (j) {
                                case 2:
                                    
                                    axis_2_and_btn.axis_2 = tempValue;
                                    break;
                                
                                case 3:

                                    axis_2_and_btn.btn1 = tempValue;
                                    break;

                                case 4:

                                    axis_2_and_btn.btn2 = tempValue;
                                    break;

                                case 5:

                                    axis_2_and_btn.btn3 = tempValue;

                                    // Emergency stop: disable all motors en relais
                                    if (axis_2_and_btn.btn3 == 1) {

                                        digitalWrite(relais_pin, LOW);
                                        disableAllMotors();

                                    }

                                    break;

                                case 6:

                                    axis_2_and_btn.btn4 = tempValue;
                                    break;

                                case 7:

                                    axis_2_and_btn.btn5 = tempValue;
                                    break;

                                case 8:

                                    axis_2_and_btn.btn6 = tempValue;
                                    break;

                                case 9:

                                    axis_2_and_btn.btn7 = tempValue;
                                    break;

                                case 10:

                                    axis_2_and_btn.btn8 = tempValue;
                                    break;

                                case 11:

                                    axis_2_and_btn.btn9 = tempValue;
                                    break;

                                case 12:

                                    axis_2_and_btn.btn10 = tempValue;
                                    break;

                                default:
                                    break;
                                }

                                xSemaphoreGive(mutex_ax_and_btn);

                            }

                            break;
                        }
                        
                        j++;

                        
                    } else  {

                        tempString += newString[i];

                    }

                    i++;

                }

            }
        } else {

            // No input over bluetooth so diable motors
            // Could be near miss between serial communication
            // Count the times there appeared to be no communication
            no_connection_counter++;

        }

        // if repeatedly (50 times) no incomming data diable turret
        if (no_connection_counter >= 50) {

            disableAllMotors();
            BlueSerial.print("ESP ready");
            startUp = true;
            no_connection_counter = 0;

            if (xSemaphoreTake(mutex_ax_and_btn, 0) == pdTRUE) {

                axis_2_and_btn.BL_Connected = false;
                xSemaphoreGive(mutex_ax_and_btn);

            }

        }

    }
}

void taskStepper_0(void * parameter) {

    byte local_axis_0;
    int step_count = 0;
    bool homed = false;
    unsigned long previous_time = 0;
    int interval;
    bool counterclockwise;
    byte MAX_STEPS = 165;
    bool drive_stepper;


    while(true) {
        
        // Copy global variable
        if (xSemaphoreTake(mutex_axis_0, 0) == pdTRUE) { 

            local_axis_0 = global_axis_0;
            xSemaphoreGive(mutex_axis_0);
            
        }

        // Check if recently homed
        if (xQueueReceive(queu_handle_homing_0, &homed, 0) == pdTRUE) {         // Value of variable in queu doesn't matter

            step_count = 0;
            // Serial.println("Stepper 0 homed.");

        }

        // Code to drive steppers...
        // step_count must be between 0 and 150

        if (local_axis_0 <= 120) {                  // Go counterclockwise
            
            digitalWrite(stepper_0_dirPin, HIGH);
            counterclockwise = true;
            drive_stepper = true;

            // Interpolation from 120 - 0 to 1 - 100 degrees/s and 1.8 degrees/step 
            // degrees/s => y = 1 + x * (100 - 1) and x = (local_axis_0 - 120) / (0 - 120)
            // s/step => 1.8 / y 
            // is in seconds -> *1000 for ms | \100 * 100 is necessary otherwise no linear output
            interval = round((1.8 / (1 * 100 + 100 * (local_axis_0 - 120) / (0 - 120) * (100 - 1))) * 1000 * 100);
            // Serial.println(interval);

        } else if (local_axis_0 >= 135) {           // Go clockwise

            digitalWrite(stepper_0_dirPin, LOW);
            counterclockwise = false;
            drive_stepper = true;

            // Interpolation from 135 - 255 to 1 - 100 degrees/s and 1.8 degrees/step 
            // degrees/s => y = 1 + x * (100 - 1) and x = (local_axis_0 - 135) / (255 - 135)
            // s/step => 1.8 / y 
            // is in seconds -> *1000 for ms | \100 * 100 is necessary otherwise no linear output
            interval = round((1.8 / (1 * 100 + 100 * (local_axis_0 - 135) / (255 - 135) * (100 - 1))) * 1000 * 100);
            // Serial.println(interval);

        } else {

            drive_stepper = false;

        }

        if (drive_stepper && millis() - previous_time >= interval) {

            previous_time = millis();

            if (counterclockwise) {

                step_count--;
                // Serial.println("-");

                if (step_count < 0) {

                    step_count = -1;

                }

            } else {

                step_count++;
                // Serial.println("+");

                if (step_count > MAX_STEPS) {

                    step_count = MAX_STEPS + 1;

                }

            }

            if (0 <= step_count && step_count <= MAX_STEPS) {
            
                stepStepper(0);
                // Serial.println(step_count);

            }

        }

        vTaskDelay(1);
       
    }

}

void taskStepper_1(void * parameter) {
    
    byte local_axis_1;
    int step_count = 0;
    bool homed = false;
    unsigned long previous_time = 0;
    int interval;
    bool counterclockwise;
    byte MAX_STEPS = 40;
    bool drive_stepper;

    while(true) {
        
        // Copy global variable
        if (xSemaphoreTake(mutex_axis_1, 0) == pdTRUE) { 

            local_axis_1 = global_axis_1;
            xSemaphoreGive(mutex_axis_1);
            
        }

        // Check if recently homed
        if (xQueueReceive(queu_handle_homing_1, &homed, 0) == pdTRUE) {         // Value of variable in queu doesn't matter

            step_count = 0;
            // Serial.println("Stepper 1 homed.");

        }

        // Code to drive steppers...
        // step_count must be between 0 and 50

        if (local_axis_1 <= 120) {                  // Go counterclockwise
            
            digitalWrite(stepper_1_dirPin, LOW);
            counterclockwise = false;
            drive_stepper = true;

            // Interpolation from 120 - 0 to 1 - 100 degrees/s and 1.8 degrees/step 
            // degrees/s => y = 1 + x * (100 - 1) and x = (local_axis_0 - 120) / (0 - 120)
            // s/step => 1.8 / y 
            // is in seconds -> *1000 for ms | \100 * 100 is necessary otherwise no linear output
            // interval = round((1.8 / (1 * 100 + 100 * (local_axis_1 - 120) / (0 - 120) * (100 - 1))) * 1000 * 100);
            // Serial.println(interval);

            // Interpolation from 120 - 0 to 1 - 50 degrees/s and 1.8 degrees/step 
            // degrees/s => y = 1 + x * (50 - 1) and x = (local_axis_0 - 120) / (0 - 120)
            // s/step => 1.8 / y 
            // is in seconds -> *1000 for ms | \100 * 100 is necessary otherwise no linear output
            interval = round((1.8 / (1 * 100 + 100 * (local_axis_1 - 120) / (0 - 120) * (50 - 1))) * 1000 * 100);            

        } else if (local_axis_1 >= 135) {           // Go clockwise

            digitalWrite(stepper_1_dirPin, HIGH);
            counterclockwise = true;
            drive_stepper = true;

            // Interpolation from 135 - 255 to 1 - 100 degrees/s and 1.8 degrees/step 
            // degrees/s => y = 1 + x * (100 - 1) and x = (local_axis_0 - 135) / (255 - 135)
            // s/step => 1.8 / y 
            // is in seconds -> *1000 for ms | \100 * 100 is necessary otherwise no linear output
            // interval = round((1.8 / (1 * 100 + 100 * (local_axis_1 - 135) / (255 - 135) * (100 - 1))) * 1000 * 100);
            // Serial.println(interval);

            // Interpolation from 135 - 255 to 1 - 50 degrees/s and 1.8 degrees/step 
            // degrees/s => y = 1 + x * (50 - 1) and x = (local_axis_0 - 135) / (255 - 135)
            // s/step => 1.8 / y 
            // is in seconds -> *1000 for ms | \100 * 100 is necessary otherwise no linear output
            interval = round((1.8 / (1 * 100 + 100 * (local_axis_1 - 135) / (255 - 135) * (50 - 1))) * 1000 * 100);

        } else {

            drive_stepper = false;

        }

         if (drive_stepper && millis() - previous_time >= interval) {

            previous_time = millis();

            if (counterclockwise) {

                step_count--;
                // Serial.println("-");

                if (step_count < 0) {

                    step_count = -1;

                }

            } else {

                step_count++;
                // Serial.println("+");

                if (step_count > MAX_STEPS) {

                    step_count = MAX_STEPS + 1;

                }

            }

            if (0 <= step_count && step_count <= MAX_STEPS) {
            
                stepStepper(1);
                Serial.println(step_count);

            }

        }

        vTaskDelay(1);
       
       
    }

}

void taskAxis_2_and_btn(void * parameter) {

    // Input variables
    byte axis_2;
    bool btn1;
    bool btn2;
    bool btn3;
    bool btn4;
    bool btn5;
    bool btn6;
    bool btn7;
    bool btn8;
    bool btn9;
    bool btn10;
    bool BL_Connected;

    // Variables for processing input
    // btn1
    bool readyToFire = false;
    byte DC_A_PWM_Standard = 155;

    // btn2
    byte DC_B_threshold = 30;
    bool lastBtn2 = 0;
    
    // btn8 
    bool boostMode = false;
    bool lastBtn8 = false;

    // btn10
    bool allMotorsDisabled = false;
    bool lastBtn10 = false;

    while (true) {

        // Copy global variable
        if (xSemaphoreTake(mutex_ax_and_btn, 0) == pdTRUE) {

            axis_2 = axis_2_and_btn.axis_2;
            btn1 = axis_2_and_btn.btn1;
            btn2 = axis_2_and_btn.btn2;
            btn3 = axis_2_and_btn.btn3;
            btn4 = axis_2_and_btn.btn4;
            btn5 = axis_2_and_btn.btn5;
            btn6 = axis_2_and_btn.btn6;
            btn7 = axis_2_and_btn.btn7;
            btn8 = axis_2_and_btn.btn8;
            btn9 = axis_2_and_btn.btn9;
            btn10 = axis_2_and_btn.btn10;
            BL_Connected = axis_2_and_btn.BL_Connected;

            xSemaphoreGive(mutex_ax_and_btn);  

        }

        // If there is bluetooth connection
        if (BL_Connected) {

            // Serial.println("CONNECTED.");
        
            if (btn8 == 1 && btn8 != lastBtn8) { // Toggle boost mode


                if (boostMode) {

                    boostMode = false;

                } else {

                    boostMode = true;

                }

            }

            lastBtn8 = btn8;

            if (!boostMode) {

                // btn2 assigned to enable/disable DC_B
                if (axis_2 >= DC_B_threshold) {

                    

                    if (btn2 == 1 && btn2 != lastBtn2) {

                        if (!readyToFire) {
                            
                            readyToFire = true;
                            ledcWrite(PWM_CHANNEL_B, axis_2);
                            // Serial.println("Firing.");
                        
                        } else {

                            readyToFire = false;
                            ledcWrite(PWM_CHANNEL_B, 0);
                            // Serial.println("Stop firing.");

                        }


                    } 

                    if (readyToFire) {      // To get dynamic speed control of DC_B

                        ledcWrite(PWM_CHANNEL_B, axis_2);

                    }

                } else {

                    readyToFire = false;
                    ledcWrite(PWM_CHANNEL_B, 0);
                    // Serial.println("Stop firing.");

                }

                lastBtn2 = btn2;

                // Btn1 assigned to the trigger
                if (btn1 == 1) {

                        // Trigger enable only if bullets can leave the gun
                        if (readyToFire) {

                            ledcWrite(PWM_CHANNEL_A, DC_A_PWM_Standard);

                        } else {

                            ledcWrite(PWM_CHANNEL_A, 0);

                        }

                } else {

                    ledcWrite(PWM_CHANNEL_A, 0);

                }

            } else if (boostMode && !allMotorsDisabled) {    // boostMode

                ledcWrite(PWM_CHANNEL_B, 255);

                if (btn1 == 1) {
                    
                    ledcWrite(PWM_CHANNEL_A, 255);
                    
                } else {

                    ledcWrite(PWM_CHANNEL_A, 0);

                }

            }
            // Btn9 assigned to home steppers
            if (btn9 == 1) {

                homingSteppers();

            }

            // Btn10 assigned to disable/enable motors
            if (btn10 == 1 && btn10 != lastBtn10) {
                
                if (allMotorsDisabled) {

                    enableAllMotors();
                    allMotorsDisabled = false;

                } else {

                    disableAllMotors();
                    allMotorsDisabled = true;
                    
                }

            }

            lastBtn10 = btn10;

        }

        vTaskDelay(1);


    }

}
// ****************************************************************

void setup() {
   
    // Begin Serial and bluetooth communication
    BlueSerial.begin("ESP32test");
    Serial.begin(115200);

    Serial.println("\nstarting up...");

    // Declare pinmodes
    pinMode(limit_axis_0_pin, INPUT_PULLDOWN);
    pinMode(limit_axis_1_pin, INPUT_PULLDOWN);

    pinMode(relais_pin, OUTPUT);
    
    pinMode(stepper_0_stepPin, OUTPUT);
    pinMode(stepper_0_dirPin, OUTPUT);
    pinMode(stepper_0_sleepPin, OUTPUT);
    pinMode(stepper_0_enablePin, OUTPUT);
    
    pinMode(stepper_1_stepPin, OUTPUT);
    pinMode(stepper_1_dirPin, OUTPUT);
    pinMode(stepper_1_sleepPin, OUTPUT);
    pinMode(stepper_1_enablePin, OUTPUT);
    
    pinMode(enable_DC_A_Pin, OUTPUT);
    pinMode(enable_DC_B_Pin, OUTPUT);

    // Create and attach PWM channels
    ledcSetup(
        PWM_CHANNEL_A,          // PWM Channel
        1000,                   // PWM frequency
        8);                     // PWM Resolution in bits
    

    ledcSetup(
        PWM_CHANNEL_B,          // PWM Channel
        1000,                   // PWM Frequency
        8);                     // PWM Resolution in bits

    ledcAttachPin(enable_DC_A_Pin, PWM_CHANNEL_A);
    ledcAttachPin(enable_DC_B_Pin, PWM_CHANNEL_B);

    // Create mutex
    mutex_axis_0 = xSemaphoreCreateMutex();
    mutex_axis_1 = xSemaphoreCreateMutex();
    mutex_ax_and_btn = xSemaphoreCreateMutex();

    // Check if mutex was created succesfully
    if( mutex_axis_0 != NULL && mutex_axis_1 != NULL && mutex_ax_and_btn != NULL) {

        Serial.println("Mutex's Created.") ; 
    
    } else {
       
        Serial.println("Failed to create mutex.");
        Serial.println("Restarting in 3 seconds.");
        delay(3000);
        ESP.restart();
    
    }

    // Create queu
    queu_handle_homing_0 = xQueueCreate(
                                1,                  // Queu size
                                sizeof(boolean));   // Itemsize
           

    queu_handle_homing_1 = xQueueCreate(
                                1,                  // Queu size
                                sizeof(boolean));   // Itemsize

    // Check is queu was created succesfully
    if (queu_handle_homing_0 != NULL && queu_handle_homing_1 != NULL) {

        Serial.println("Queus created.");

    } else {

        Serial.println("Failed to create queus.");
        Serial.println("Restarting in 3 seconds.");
        delay(3000);
        ESP.restart();

    }

    // Pin taskt to corresponding core
    BaseType_t xReturned_Bluetooth;
    BaseType_t xReturned_Stepper_0;
    BaseType_t xReturned_Stepper_1;
    BaseType_t xReturned_axis_2_and_btn;

    xReturned_Bluetooth = xTaskCreatePinnedToCore(
                                            taskBluetooth,           // Task function. 
                                            "Task_Bluetooth",       // name of task. 
                                            1000,                   // Stack size of task 
                                            NULL,                   // parameter of the task 
                                            1,                      // priority of the task 
                                            &taskhandle_Bluetooth,  // Task handle to keep track of created task 
                                            0);                     // pin task to core 0  

    xReturned_Stepper_0 = xTaskCreatePinnedToCore(
                                            taskStepper_0,          // Task function. 
                                            "Task_Stepper_0",       // name of task. 
                                            900,                    // Stack size of task 
                                            NULL,                   // parameter of the task 
                                            2,                      // priority of the task 
                                            &taskhandle_Stepper_0,  // Task handle to keep track of created task 
                                            1);                     // pin task to core 1

    xReturned_Stepper_1 = xTaskCreatePinnedToCore(
                                            taskStepper_1,          // Task function. 
                                            "Task_Stepper_1",       // name of task. 
                                            900,                    // Stack size of task 
                                            NULL,                   // parameter of the task 
                                            2,                      // priority of the task 
                                            &taskhandle_Stepper_1,  // Task handle to keep track of created task 
                                            1);                     // pin task to core 1
    
    xReturned_axis_2_and_btn = xTaskCreatePinnedToCore(
                                            taskAxis_2_and_btn,     // Task function. 
                                            "Task_Axis_2_and_btn",  // name of task. 
                                            900,                    // Stack size of task 
                                            NULL,                   // parameter of the task 
                                            1,                      // priority of the task 
                                            &taskhandle_Axis_2_and_btn,  // Task handle to keep track of created task 
                                            1);                     // pin task to core 1

    // Check if tasks were created succesfully
    if (xReturned_Bluetooth == pdTRUE && xReturned_Stepper_0 == pdTRUE && xReturned_Stepper_1 == pdTRUE && xReturned_axis_2_and_btn == pdTRUE) {

        Serial.println("Task created.");
        
        // Suspend tasks to focus on setup
        vTaskSuspend(taskhandle_Stepper_0);
        vTaskSuspend(taskhandle_Stepper_1);
        vTaskSuspend(taskhandle_Axis_2_and_btn);
        vTaskSuspend(taskhandle_Bluetooth);

    } else {

        Serial.println("Failed to create tasks.");
        Serial.println("Restarting in 3 seconds.");
        delay(3000);
        ESP.restart();

    }

    delay(1000);
    digitalWrite(relais_pin, HIGH);
    homingSteppers();
    disableAllMotors();
    initiateBluetoothCommunication();

    // Resume tasks
    vTaskResume(taskhandle_Bluetooth);
    vTaskResume(taskhandle_Stepper_0);
    vTaskResume(taskhandle_Stepper_1);
    vTaskResume(taskhandle_Axis_2_and_btn);
    

    Serial.println("Setup Done.");
    delay(1000);
    
    vTaskDelete(NULL);                      // Delete void setup() and void loop()
}


void loop() {} // Empty loop, will be deleted

